const express = require('express')

const dotenv = require('dotenv')

const mongoose = require('mongoose')

const app = express()

const port = 4001

dotenv.config()

// Mongoose Connection
mongoose.connect(`mongodb+srv://RuRuSensei:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.5jpfbgk.mongodb.net/S35-Activity?retryWrites=true&w=majority`, 
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
)

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))


// Schema

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

// Model

const User = mongoose.model('User', userSchema)


// Express Middleware
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))

// ROUTES

// Creating a user - route
app.post('/signup', (req, res) => {
	User.findOne({username: req.body.username}, (error, result) => {
		if(result != null && result.username == req.body.username) {
			return res.send('A duplicate user found!')
		} else {
			if(req.body.username !== '' && req.body.password !== '') {

				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})

				newUser.save((error, savedUser) => {
					if(error) {
						return res.send(error)
					}
					return res.send('New user registered!')
				})
			} else {
				return res.send('Please provide a Username and Password.')
			}
		}
	})
})

// Port
app.listen(port, () => console.log(`Server is running at port: ${port}`))
